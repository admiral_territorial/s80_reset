## S80 Rules of Engagement

All ships are valid target unless protected as follows:

### Survey Ships
* UPC Survey Ships actively mining on a node, and that are not engaged in any other activities, are protected.
* Z-Nodes are fair game with no warning given.
* **Exception**: Survey Ships scoring Event Points towards an active SLB/ALB are legal targets.  (This exception does not apply to Mining SLBs/ALBs.)

### Armadas
* Ships heading to or participating in an active armada circle are protected.
* Ships choosing to participate in other activities in that system forfeit that protection.
* Stealing armadas via speedups is not allowed.

### Token Space
* Ships have blanket protection in Token Space.
* **Exception**: Non-Voyager ships can be killed if they are occupying a mining node in Delta Quadrant Token Space.

### Territory
* UPC Survey Ships on mining nodes are protected, even if Z-Node.
* If a TC is contested (2+ alliances can score), then all ships are legal targets.

### War
War voids all ROE protections.