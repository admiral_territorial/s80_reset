### Alliance leadership commits to firmly establish a healthy player culture where player interactions and communications both in-game and on Discord are conducted in accordance to an agreed upon code of conduct that prohibits negative and harmful behaviors focused towards individual players and/or groups of players.

#### Prohibited Behavior:
* No threats, harassment, and other violations of the S80 rules including but not limited to name calling, racism, sexism, genderism, antisemitism, homophobia, transphobia, xenophobia, ableism, ageism, and bullying.
* No inappropriate or derogatary references to a player’s personal life, interests, family members, pets, etc.

#### Code of Conduct Violations:
* Players who have observed or experienced harassment in-game or on Discord can choose to attempt resolution directly with the other player or work with leadership to report and resolve the issue through diplomacy channels.
* Repeat violators or players who clearly communicate an understanding of and intention to flagrantly disregard the agreed upon code of conduct will be escalated to alliance leadership to align on next steps for resolution (which may be as severe as being asked to remove the player from the alliance). Additionally, these players will be silenced or banned from the platform the behavior was observed on, where possible.
* Alliances that knowingly host a player who, given multiple warnings, actively and demonstratively continues to violate the server code of conduct will face possible action as formally declared by the leadership of impacted alliance(s). This action may be levied against the individual player(s) in egregious violation of the agreed upon rules and/or members of the host alliance.
 
### We commit to ensure the server-voted Rules of Engagement are respected, followed and enforced while providing an inclusive forum to discuss and manage disputes and recommended changes to the ROE standards that all signing alliances agree to follow.

#### ROE Amendments:
* A transparent and inclusive process will be established with input from all signing alliances to manage the submission, review, and approval of amendments and/or additions to the shared ROE that all signing parties agree to follow

#### ROE Violations:
* Players who have observed an ROE violation against them are encouraged to attempt to reach out to the other player, clarify whether an ROE violation has taken place and discuss if remuneration is due. If the matter is not resolved between the players directly, alliance leadership will use diplomacy channels to investigate and resolve the violation (if applicable).
* Repeat violators or players who clearly communicate an understanding of and intention to flagrantly disregard ROE will be escalated to alliance leadership to align on next steps for resolution (which may be as severe as being asked to remove the player from the alliance).
* Alliances that knowingly host a player who, given multiple warnings, continues to violate the server ROE will face possible action as formally declared by the leadership of  impacted alliance(s). This action may be levied against the individual player(s) in egregious violation of the agreed upon rules and/or all members of the host alliance.

#### ROE Violation Disputes:
* If a scenario occurs where the documented ROE does not clearly confirm or deny that a violation took place, a player can dispute the violation made against them. Disputes will be reviewed jointly by alliance leadership any recommended ROE changes will be handled through the amendment process. Players will be advised to avoid the reproducing the disputed violation until the ROE has been clarified and communicated.
 
### We commit to actively engage in activities and initiatives that provide an environment and create opportunities that celebrate all types of STFC gameplay including PVP focused activities.
* OrbitalPvP is a dedicated competitive PvP discord server focused on bracketed PvP combat.
* A “Server Brawl” may be implemented with a dedicated timeframe and location (system specific based on player and/or ship power levels) for PvP combat
* Scopley PVP events will be leveraged to create opportunities for PVP and members will be encouraged to participate in and/or plan PVP parties to maximize player PVP interactions within the designated systems at specific times.
> Our commitment to provide a structured environment for player PVP will continue to be refined and modified to include additional details and boundaries to protect and celebrate the different play styles and interests of our server population.